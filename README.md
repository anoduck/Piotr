<h1 align="center">Welcome to Piotr 👋</h1>
<h2 align="center">Get your data from [Otter.ai](https://otter.ai) with style!</h2>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Get your data from [Otter.ai](https://otter.ai) with style!

### ⚡️ [Piotr](https://codeberg.org/anoduck/piotr)

## Description

Piotr, actually pronounced "pie-otter", is a python script that performs numerous tasks with speeches from [Otter.ai](https://otter.ai). 

### Fetch

Fetching data on speeches from Otter.ai is a little more complex than one might expect, this has to deal with the return format of the dataset, which is a pandas dataframe. This dataframe then has to be filtered to remove unwanted items, such as empty entries and nonconforming data entries. Finally this filtered dataframe has to be exported to a sqlite3 database and to a csv file.

The script also performs some basic filtering operations on the dataset, like removing unwanted columns that are not needed and would just clutter up your dataset. The full list of filters is below:
1. Removes entries based on min and max duration
2. Removes entries based on undesired titles
3. Cleans up date formatting
4. Removes entries with empty content
5. Tries to replace entries with fowl language, because let's face it, we all use it.
6. Removes empty rows

### Page

The `piotr` script takes a dataframe and generates a timeline using the [pyvis](https://pyvis.readthedocs.io/en/latest/) library.

### Timeline

Specifically written in order to be used with the [Hugo](https://gohugo.io) static site generator, this script generates a yaml file that can be used to generate a timeline of speeches.

### Download

This allows the user to take a previously fetched dataset, in the form of a csv file, and download all of the transcribed speech files to a predefined folder.

### Raw

Was primarily created for developmental reasons, and simply exports unfiltered data to a sqlite3 database.

## Install

As tradition, python scripts are written using poetry, and therefore allow the user to use poetry or any of the other virtual environment management programs. If for some reason the `requirements.txt` file is not up to date, the user can run `poetry export > requirements.txt` to recreate it.

```sh
poetry install
# OR
pipenv install
```

## Usage

Understanding that not every user will require the exact same specifications as the author, the script was written to allow parameters to be customized in order to meet the needs of the user. The downside to this is configuration has been divided into two parts. First, is the configuration file that stores variables that are not optimum for inclusion as command arguments, and secondly, are the command arguments that would require ease of accessibility. Below is a summary of each of these two means of configuration and usage.

### Command Line

```bash
poetry run python piotr/piotr.py '<$ACTION>' '<$ARGS>'
```

Running `poetry run python piotr/piotr.py --help` will provide the user with a basic understanding of the options available for use on the command line.

### Command Arguments

```python
class Options:
     """ Configuration options for piotr. 
    Actions to perform:
    1. fetch - downloads the speeches data from otter.ai to a csv file.
    2. page - generates an html timeline using pyvis. (Original functionality)
    3. timeline - generates a yaml file intended for use with hugo (https://gohugo.io)
    4. download - downloads transcribed speech files.
    5. raw - exports raw data to sqlite database. (for development purposes)
    """
    action: str = choice("fetch",
                         "page",
                         "timeline",
                         "download",
                         "raw",
                         default=str("fetch"))

    config: str = "piotr.ini"  # Path to config file
    logfile: str = "piotr.log"  # Path to log file
    loglevel: str = "DEBUG"  # Log level
    db: str = "otter.db"  # Database to export your speeches to.
    rawdb: str = "otter_raw.db"  # Database to export raw otter.ai data to.
    downdir: str = "downloads"  # Folder to download speech files to.
```

### Configuration File

The configuration file is parsed using configobj, and as such will be generated for the user automatically upon first execution of this script. The configuration file will then be located in the base directory of this repository. In order to run the script, the username and password must be changed in this configuration file.

```python
## Configuration file for Piotr
## https://github.com/anoduck/Piotr
## MIT License = 'Copyright (c) 2024 Anoduck'
## This software is released under the MIT License.
## https: //opensource.org/licenses/MIT
## ----------------------------------------------------------------------------
## Please do not leave username and password unmodified and without quotes.
## --------------------------------------------------------------------------------
## \*L00k*/ -- WARNING: ALL VALUES MUST BE CONTAINED IN SINGLE QUOTES -- \*L00k*/
## --------------------------------------------------------------------------------
## Alright, we know it was not generated that way. Jeez... FML...
## ----------------------------------------------------------------------------

# Your otter username
username = string(default='replace with otter.ai username')

# Your otter password
password = string(default='replace with otter.ai password')

# Title for generated html page
title = string(default='Timeline created by Piotr')

# Start date to retrieve oldest entries
start_date = string(default='2019-09-16')

# End date to retrieve newest entries
end_date = string(default='2023-01-01')

# File name and path for generated html file
html_output = string(default='index.html')

# File name and path for downloaded speeches file
down_output = string(default='speeches.csv')

# Number of speeches to download
page_size = integer(min=45, max=9999, default=1400)

# Additional unwanted titles to exclude from the timeline
unwanted_titles = list(default=list('class', 'personal', 'idears', 'passwords', 'sensitive'))

"""

## Author

👤 **Anoduck**

* Website: http://anoduck.github.io
* Github: [@anoduck](https://github.com/anoduck)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_