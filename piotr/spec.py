#!/usr/bine/env python3
# -*- coding: utf-8 -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https: //opensource.org/licenses/MIT

cfg = """
## Configuration file for Piotr
## https://github.com/anoduck/Piotr
## MIT License = 'Copyright (c) 2024 Anoduck'
## This software is released under the MIT License.
## https: //opensource.org/licenses/MIT
## ----------------------------------------------------------------------------
## Please do not leave username and password unmodified and without quotes.
## ----------------------------------------------------------------------------
## |L00k| -- WARNING: ALL VALUES MUST BE CONTAINED IN SINGLE QUOTES -- |L00k|
## ----------------------------------------------------------------------------
## Alright, we know it was not generated that way. Jeez... FML...
## ----------------------------------------------------------------------------

# Your otter username
username = string(default='replace with otter.ai username')

# Your otter password
password = string(default='replace with otter.ai password')

# Title for generated html page
title = string(default='Timeline created by Piotr')

# Start date to retrieve oldest entries
start_date = string(default='2019-09-16')

# End date to retrieve newest entries
end_date = string(default='2023-01-01')

# File name and path for generated html file
html_output = string(default='index.html')

# File name and path for downloaded speeches file
down_output = string(default='speeches.csv')

# Number of speeches to download
page_size = integer(min=45, max=9999, default=1395)

# Additional unwanted titles to exclude from the timeline
unwanted_titles = list(default=list('class', 'personal', 'idears', 'passwords', 'sensitive'))

"""
