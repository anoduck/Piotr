#!/usr/bine/env python3
# -*- coding: utf-8 -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https: //opensource.org/licenses/MIT
from pandas.core.api import DataFrame
from otterai import OtterAI, OtterAIException
from clean import CleanUp
import pandas as pd
import chardet
import ast
import sys

# =================================================================================
# BE EXTREMELY CAREFUL EDITING THIS LIST. DO NOT MODIFY UNLESS YOU KNOW
# EXACTLY WHAT YOU'RE DOING.
# ---------------------------------------------------------------------------------
# Any error here will result in either unwanted data placed in your database, or
# may cause piotr to not behave as expected, or it may not work at all.
# =================================================================================
unwanted_columns = ['modified_time', 'deleted', 'from_shared', 'shared_with', 'unshared', 'shared_by', 'shared_groups', 'can_edit', 'can_comment', 'is_read', 'process_finished', 'upload_finished', 'hasPhotos', 'transcript_updated_at', 'images', 'live_status', 'live_status_message', 'folder', 'created_at', 'access_seconds', 'appid', 'can_highlight', 'create_method', 'otid', 'can_export', 'timecode_offset', 'timezone', 'access_request', 'public_view', 'has_started', 'auto_record', 'displayed_start_time', 'meeting_otid', 'allow_transcript_copy', 'process_failed', 'calendar_meeting_id', 'permissions', 'speech_outline_status', 'speech_metadata', 'process_status', 'access_status', 'speech_settings', 'shared_emails', 'link_share', 'calendar_guests', 'speakers', 'is_low_confidence', 'image_urls', 'sales_call_qualified', 'non_member_shared_groups', 'audio_enabled', 'auto_snapshot_enabled', 'pubsub_jwt', 'conf_join_url', 'conf_image_url', 'chat_status', 'owner', 'public_share_url', 'is_meeting_series', 'has_meeting_series_access', 'is_low_confidence', 'image_urls', 'sales_call_qualified']
# pd.set_option('future.no_silent_downcasting', True)


class FetchSpeeches:
    def __init__(self):
        self.otter = OtterAI()

    def down_data(self, username, password, page_size, log):
        """ downloads data and returns dataframe """
        self.otter.login(username, password)
        print('Fetching current page size of: ' + str(page_size))
        log.info('Fetching current page size of: ' + str(page_size))
        try:
            speeches = self.otter.get_speeches(page_size=page_size)
        except OtterAIException:
            print('Otter AI failed to login')
            sys.exit()
        return speeches

    def gen_entry_file(self, username, password, badtitle, priv, down_output, page_size, start_date, end_date, db, log):
        """ get and parse speech data """
        log.debug(f'Variable for db is: {db}')
        speeches = self.down_data(username, password, page_size, log)
        log.debug(f'Speeches is type: {type(speeches)}')
        log.debug(f'Keys of speeches is: {speeches.keys()}')
        log.info('Fetched ' + str(len(speeches)) + ' entries')
        data = speeches.get('data')
        spks = pd.DataFrame(data)
        log.debug(f'Dataframe columns: {spks.columns}')
        # spks_dicts = spks.loc['speeches', 'data']
        # spkdf = pd.DataFrame(spks_dicts)
        spkdf = spks
        cleanup = CleanUp()
        clean_df = cleanup.clean(spkdf, badtitle, priv, start_date, end_date, log)
        # Reindex and sort
        clean_df.reindex()
        clean_df.sort_values(by=['start'], inplace=True)
        clean_df.infer_objects().dtypes
        log.info('Remaining entries after reindexing: ' + str(len(clean_df)))
        log.info('Replacing blank cells with NaN')
        clean_df['title'] = clean_df['title'].replace(r'^s*$', float('NaN'), regex = True)
        clean_df['speech_outline'] = clean_df['speech_outline'].replace(r'^s*$', float('NaN'), regex = True)
        log.info('Writing to file: ' + down_output)
        clean_df.to_csv(down_output, index=False, encoding='utf-8')
        with open(down_output, 'rb') as rf:
            result = chardet.detect(rf.read())
            log.debug(f'Character Set identified as: {result["encoding"]}')
        print('Wrote speeches to file.')

    def gen_worddf(self, entry):
        master_dict = {}
        bad_words = ['case', 'class', 'opiates', 'fuckers', 'assholes', 'fuck']
        ast_list = ast.literal_eval(entry)
        group_list = []
        score_list = []
        for item in ast_list:
            raw_word = item.get('word')
            gword = raw_word.replace("'", "")
            if gword in bad_words:
                wc_word = gword.join('__')
            else:
                wc_word = gword
            wc_score = item.get('score')
            wc_fixed = float(wc_score) * 1000
            wc_split = str(wc_fixed).split('.')
            wc_prime = int(wc_split[0], base=10)
            if wc_prime > 1 and len(wc_word) > 2:
                group_list.append(wc_word)
                score_list.append(wc_prime)
        master_dict['group'] = group_list
        master_dict['score'] = score_list
        word_df = pd.DataFrame.from_dict(master_dict)
        return word_df


    def gen_groups(self, entries):
        group_list = []
        bad_rows = []
        for row in entries.itertuples():
            row_name = row[0]
            entry = row.word_clouds
            dt_df = self.gen_worddf(entry)
            try:
                word_hs = dt_df.loc[dt_df['score'].idxmax()]
                word_list = word_hs.values
                word = word_list[0]
                group_list.append(word)
            except ValueError:
                print('Dropped entry due to dtype ValueError')
                bad_rows.append(row_name)
                pass
        return group_list, bad_rows

    def gen_raw(self, username, password, rawdb, log):
        log.info('Downloading raw data and exporting to sqlite3 db')
        speeches = self.down_data(username, password, rawdb, log)
        data_dict = speeches.get('data')
        raw_data_list = data_dict.get('speeches')
        otterdf = pd.DataFrame(raw_data_list)
        otterdf = otterdf.convert_dtypes()
        otterdf = otterdf.map(str)
        conn = sqlite3.connect('raw_data.db')
        otterdf.to_sql('speeches', conn, if_exists='replace', index=False)
        conn.close()
        log.info('Done!')
        print('Done!')
