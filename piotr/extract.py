#!/usr/bine/env python3
# vim:set filetype=python
# -*- coding: utf-8 -*-
# -*- mode: python -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https://anodock.mit-license.org
from zipfile import ZipFile, is_zipfile
from alive_progress import alive_it
import os


class ExtractFiles:

    def getExt(self, fullzip, exft, log):
        with ZipFile(fullzip, 'r') as rz:
            zlist = rz.namelist()
        sel = ""
        for item in zlist:
            if exft in item:
                sel = item
        log.debug(f'Found {sel}')
        return sel


    def get_new_dir(self, tpath, zbase, log):
        less_ext = zbase.split('.')[0]
        log.debug(f'Dir name should be: {less_ext}')
        log.debug(f'Target Dir remains {tpath}')
        zpath = os.path.join(tpath, less_ext)
        log.debug(f'Extractraction dir {less_ext} will be {zpath}')
        if not os.path.isdir(zpath):
            os.mkdir(zpath)
        return zpath

    def makeNewFile(self, tpath, fname, zbase, exft, log):
        rpath = os.path.join(tpath, fname)
        log.debug(f'Extraction result location {rpath}')
        zless = zbase.split('.')[0]
        newName = zless + '.' + exft
        log.debug(f'New name will be {newName}')
        new_path = os.path.join(tpath, newName)
        log.debug(f'New path should be {new_path}')
        os.rename(rpath, new_path)
        if os.path.isfile(new_path):
            return new_path
        else:
            print("Oops, that didn't work.")
            exit(1)

    def extract(self, downdir, exft, tdir, log):
        log.info('Beginning the extraction process.')
        dirpath = os.path.abspath(downdir)
        log.debug(f'looking for files in {dirpath}')
        tpath = os.path.abspath(tdir)
        if not os.path.isdir(tpath):
            os.mkdir(tpath)
        log.debug(f'Will extract files into {tpath}')
        log.info(f'Discovered {len(os.listdir(dirpath))} files.')
        flist = os.listdir(dirpath)
        log.debug(f'File list is: {flist}')
        for zip in alive_it(flist):
            log.debug(f'File to extract: {zip}')
            fullzip = os.path.join(dirpath, zip)
            log.debug(f'Absolute path to file is {fullzip}')
            if is_zipfile(fullzip):
                zbase = os.path.basename(fullzip)
                log.debug(f'Basename of file: {zbase}')
                log.debug(f'Chosen extension is: {exft}')
                if exft == 'all':
                    zpath = self.get_new_dir(tpath, zbase, log)
                    log.debug(f'Will be using {zpath} for {fullzip}')
                    with ZipFile(fullzip, 'r') as zf:
                        zf.extractall(zpath)
                    log.debug(f'Extracted all to {zpath}')
                else:
                    fname = self.getExt(fullzip, exft, log)
                    with ZipFile(fullzip) as zf:
                        zf.extract(fname, tpath)
                    new_path = self.makeNewFile(tpath, fname, zbase, exft, log)
                    log.debug(f'Extracted {fname} to path {new_path} from file {fullzip}')
