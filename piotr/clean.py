#!/usr/bin/env python3
import os
import pandas as pd
from ucolumns import unwanted_columns
from numpy import nan, array
from datetime import datetime

class CleanUp:

    # Date Filtering
    def filter_date(self, white_df, start_date, end_date, log):
        white_df['start'] = pd.to_datetime(white_df['start'])
        white_df['end'] = pd.to_datetime(white_df['end'])
        dt_format = '%Y-%m-%d'
        startval = datetime.strptime(start_date, dt_format)
        endval = datetime.strptime(end_date, dt_format)
        start_array = array(white_df.start.dt.to_pydatetime())
        end_array = array(white_df.end.dt.to_pydatetime())
        array_mask = (start_array >= startval) & (end_array <= endval) # Generates deprecated warning
        white_df = pd.DataFrame(white_df[array_mask])
        log.info('Remaining white_df after date filtering: ' + str(len(white_df)))
        white_df['start'] = pd.to_datetime(white_df.start).dt.strftime(dt_format) # Generates deprecated warning
        white_df['end'] = pd.to_datetime(white_df.end).dt.strftime(dt_format) # Generates deprecated warning
        return white_df

    # Duration filtering
    def filter_duration(self, dated_df, log):
        log.info('Speech Duration Filtering')
        min_dur = 3
        max_dur = 10800
        dur_array = dated_df.duration.to_numpy()
        dur_mask = (dur_array >= min_dur) & (dur_array <= max_dur)
        new_df = pd.DataFrame(dated_df[dur_mask])
        return new_df

    # Date Formatting cleanup
    def clean_format(self, frshdf, log):
        log.info('Date Formatting cleanup')
        dt_format = '%Y-%m-%d'
        log.info('Before cleanup datetime format was: {}'.format(frshdf['start'][1]))
        start_dates = pd.to_datetime(frshdf['start'], dt_format, unit='s')
        end_dates = pd.to_datetime(frshdf['end'], dt_format, unit='s')
        frshdf.replace({'start': frshdf.start.values}, {'start': start_dates.values}, inplace=True)
        frshdf.replace({'end': frshdf.end.values}, {'end': end_dates.values}, inplace=True)
        new_content = frshdf.content.str.replace("'", "")
        frshdf.replace({'content': frshdf.content.values}, {'content': new_content.values}, inplace=True)
        return frshdf

    # remove unwanted titles
    def clean_titles(self, clndf, badtitle, priv, log):
        log.info('Redacting rows with unwanted titles')
        priv_words = []
        priv_path = os.path.abspath(priv)
        with open(priv_path, 'r') as rs:
            for line in rs.readlines():
                priv_words.append(line.strip())
        redact_list = priv_words + badtitle
        titlelist = clndf.title.to_list()
        for value in titlelist:
            if value is None:
                continue
            else:
                for word in redact_list:
                    if word in value:
                        def m(word): return "X" * len(word)
                        newval = value.replace(word, m(word))
                        clndf.replace(value, newval, inplace=True)
        return clndf

    # Data Cleanup
    def data_cleanup(self, dirty_df, log):
        log.info('Data Cleanup')
        no_empty = dirty_df.dropna(axis=0, how='any', subset=[
                                'word_clouds', 'start_time', 'summary', 'end_time'])
        ftdf = no_empty.rename(
            columns={"summary": "content", "start_time": "start", "end_time": "end"})
        log.debug('Dataframe columns reamaining: '.format(ftdf.columns))
        log.info('Dropping rows with empty content')
        ftdf.replace({'content':''}, {'content': nan}, inplace=True)
        ftdf.dropna(subset=['content'], inplace=True)
        return ftdf

    def clean(self, spkdf, badtitle, priv, start_date, end_date, log) -> object:
        log.info('_-_-_-_-_-_-_-_-_-_-_-_-_-_-_')
        log.info('Begin the cleanup process:')
        log.info('_-_-_-_-_-_-_-_-_-_-_-_-_-_-_')
        dirty_df = spkdf.drop(columns=unwanted_columns)
        log.info('Remaining entries after droping empty content: ' + str(len(dirty_df)))
        clndf = self.data_cleanup(dirty_df, log)
        log.info('Remaining entries after droping unwanted titles: ' + str(len(clndf)))
        frshdf = self.clean_titles(clndf, badtitle, priv, log)
        log.info('Remaining entries after droping unwanted titles: ' + str(len(frshdf)))
        dated_df = self.clean_format(frshdf, log)
        log.info('Remaining entries after format cleanup: ' + str(len(dated_df)))
        fltd_df = self.filter_duration(dated_df, log)
        log.info('Remaining fltd_df after duration filtering: ' + str(len(fltd_df)))
        prcd_df = self.filter_date(fltd_df, start_date, end_date, log)
        log.info('After date filtering, datetime format is: {}'.format(prcd_df['start'].values[1]))
        return prcd_df
