#!/usr/bine/env python3
# -*- coding: utf-8 -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https: //opensource.org/licenses/MIT
from random import choice
from yaml import dump
import pandas as pd


class Timeline:

    def __init__(self):
        self.events = []
        self.icon_list = ['fa-location-arrow', 'fa-play', 'fa-recycle', 'fa-hand-o-right',      'fa-ambulance', 'fa-bed',
                          'fa-anchor', 'fa-building', 'fa-car', 'fa-compass', 'fa-home', 'fa-road', 'fa-street-view',
                          'fa-truck', 'fa-comment', 'fa-envelope', 'fa-inbox', 'fa-send', 'fa-star', 'fa-image', 'fa-cogs',
                          'fa-refresh', 'fa-wrench', 'fa-tree', 'fa-info', 'fa-question', 'fa-chain-broken', 'fa-legal',
                          'fa-clock-o', 'fa-diamond', 'fa-hourglass', 'fa-blind', 'fa-institution', 'fa-gavel',
                          'fa-briefcase', 'fa-users', 'fa-user', 'fa-thumbs-down', 'fa-frown-o', 'fa-user-secret']

    def create_timeline(self, down_output):
        newdf = pd.read_csv(down_output, usecols=[
                            'speech_id', 'start', 'content', 'word_clouds'])
        for _, row in newdf.iterrows():
            bad_date = row.iloc[1]
            date = bad_date.split(" ")[0]
            stryear = date.split("-")[0]
            year = stryear.strip("'")
            ricon = choice(self.icon_list)
            icon = ricon
            title = row.iloc[0]
            content = row.iloc[2]
            newdict = {'icon': icon, 'date': date,
                    'title': title, 'content': content}
            if newdict not in self.events:
                self.events.append(newdict)
        with open('hugo_events.yml', 'w') as file:
            dump(self.events, file)
        print(open('hugo_events.yml').read())
