#!/usr/bine/env python3
# vim:set filetype=python
# -*- coding: utf-8 -*-
# -*- mode: python -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https://anoduck.mit-license.org
from otterai import OtterAI, OtterAIException
from alive_progress import alive_it
import pandas as pd
import os
import sys
import chardet


class DownSpeeches:

    def __init__(self) -> None:
        self.otter = OtterAI()

    def check_dir(self, downdir):
        """Make sure directory exists"""
        check_path = os.path.abspath(downdir)
        if not os.path.isdir(check_path):
            os.mkdir(check_path)
        return check_path

    def return_list(self, spkdf, log):
        """Acquire Speech ID from dataframe."""
        log.debug(f'Available columns: {spkdf.columns}')
        log.debug(f'Current contents of download_url column: {spkdf['download_url'].values.tolist()}')
        spklst = spkdf['download_url'].values.tolist()
        id_list = []
        count = len(spklst)
        for i in range(0, count):
            url = spklst[i]
            parts = url.split('/')
            id = parts[-1]
            id_list.append(id)
        return id_list

    def get_df(self, db, log):
        """Acquire dataframe from csv file."""
        with open(db, 'rb') as rf:
            log.info(f'File to read is {db}')
            result = chardet.detect(rf.read())
        log.debug(f'Charset detected: {result["encoding"]}')
        spkdf = pd.read_csv(db, encoding=result['encoding'], on_bad_lines='skip')
        log.debug(f'Length of read dataframe: {len(spkdf)}')
        return spkdf

    def get_speeches(self, db, downdir, username, password, log):
        """Download speeches from otter.ai naming them with speech id."""
        self.username = username
        self.password = password
        downdir = self.check_dir(downdir)
        if os.path.isdir(downdir):
            pass
        else:
            print('Failed to create download dir')
            sys.exit(1)
        try:
            self.otter.login(self.username, self.password)
        except OtterAIException:
            print('Otter AI failed to login')
            sys.exit()
        self.log = log
        spkdf = self.get_df(db, log)
        spklst = self.return_list(spkdf, log)
        self.log.debug(f'list length: {len(spklst)}')
        self.log.debug(f'list type: {type(spklst)}')
        for spkid in alive_it(spklst):
            self.log.debug(f'Processing: {spkid}')
            spkname = os.path.join(downdir, spkid)
            self.log.debug(f'Name: {spkname}')
            try:
                response = self.otter.get_speech(spkid)
            except OtterAIException:
                print('speech location error: {}'.format(spkid))
                sys.exit()
            try:
                response = self.otter.download_speech(spkid,
                                                      name=spkname)
            except OtterAIException:
                print('Otter AI failed to download')
                print('Download Error: {}'.format(response))
                self.log.error('Otter AI failed to download')
                sys.exit()
            """ if os.path.exists(spkname): """
            """     continue """
            """ else: """
            """     print(f'Error! Nothing was downloaded for {spkname}') """
            """     sys.exit(1) """
        print("DO NOT FORGET TO REDACT YOUR PDF FILES!")
