#!/usr/bine/env python3
# -*- coding: utf-8 -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https://anoduck.mit-license.org

from simple_parsing import parse, choice
from configobj import ConfigObj
from configobj.validate import Validator
from dataclasses import dataclass
import os
import sys
from proclog import OtLog
from spec import cfg
from fetch import FetchSpeeches
from othtml import GenHTML
from timeline import Timeline
from tablemaker import MakeTables
from down import DownSpeeches
from extract import ExtractFiles

@dataclass
class Options:
    """ Configuration options for piotr.
    Actions to perform:
    1. fetch - downloads the speeches data from otter.ai to a csv file.
    2. vispage - generates an html timeline using pyvis. (Original functionality)
    3. timeline - generates a yaml file intended for use with hugo (https://gohugo.io)
    4. markdown - generates a markdown table from previously fetched csv
    5. download - downloads transcribed speech files in multiple formats in zip archive.
    6. extract - extracts files from the downloaded archive.
    7. raw - exports data directly to csv without filtering. (for development purposes)
    """
    action: str = choice("fetch",
                         "vispage",
                         "timeline",
                         "markdown",
                         "download",
                         "extract",
                         "raw",
                         default=str("fetch"))  # Action, i.e. What to do.

    config: str = "piotr.ini"  # Path to config file
    logfile: str = "piotr.log"  # Path to log file
    loglevel: str = "DEBUG"  # Log level
    gendb: bool = False  # Generate database. False by default. /DEPRECATED/
    db: str = "speeches.csv"  # csv file to export your speeches to.
    priv: str = "sensitive.txt"  # File containing one sensitive phrase per line to redact.
    downdir: str = "otter-data"  # Folder to download speech files to.
    webpath: str = "otter-logs"  # Relative web path to downloaded speech dirs.
    rawcsv: str = "otter_raw.csv"  # Csv file to export raw otter.ai data to.
    tdir: str = ""  # Directory to extract zip archives into. (Default is downdir)
    exft: str = choice("pdf", "html", "txt", "docx", "srt", "mp3", "all",
                       default=str("all"))  # What filetype do you want extracted?
    link: str = choice("md", "link", "ref", default=str("md"))  # Markdown standard, Hugo Link, or Hugo Reference?
    output: str = "otter.md"  # Output markdown file.

Options = parse(Options, dest="Options")


class Piotr:
    def __init__(self, Options):
        self.Options = Options
        self.vader = Validator()

    def redir_path(self, file):
        real_file = os.path.basename(file)
        parent_path = os.path.abspath(os.path.join('./', os.pardir))
        file_path = os.path.join(parent_path, real_file)
        return file_path

    def process_config(self):
        self.spec = cfg.split("\n")
        if os.path.isfile(self.conf_file):
            self.conf_file = os.path.realpath(self.conf_file)
            self.conf = ConfigObj(self.conf_file, configspec=self.spec)
            self.log.info('Configuration file read from {}'.format(self.conf_file))
            # return self.conf
        else:
            self.conf = ConfigObj(self.conf_file, configspec=self.spec)
            self.conf.filename = self.conf_file
            self.conf.validate(self.vader, copy=True)
            self.conf.write()
            print("Configuration file written to {}".format(self.conf_file))
            sys.exit()

    def main(self):
        """ The main function of script...yada yada yada... """
        self.conf_file = self.Options.config
        self.logfile = self.Options.logfile
        self.lev = self.Options.loglevel
        self.log_class = OtLog(log_file=self.logfile, lev=self.lev)
        self.log = self.log_class.get_log()
        self.priv = self.Options.priv
        self.downdir = self.Options.downdir
        self.exft = self.Options.exft
        self.tdir = self.Options.tdir
        self.priv = self.Options.priv
        self.link = self.Options.link
        self.webpath = self.Options.webpath
        if self.tdir == "":
            self.tdir = self.downdir
        else:
            self.tdir = self.Options.tdir
        self.process_config()
        username = self.conf['username']
        if username == 'replace with otter.ai username':
            print('Please edit piotr.ini and replace username with your otter.ai username')
            sys.exit()
        password = self.conf['password']
        title = self.conf['title']
        start = self.conf['start_date']
        end = self.conf['end_date']
        html_output = self.conf['html_output']
        down_output = self.conf['down_output']
        html_output = os.path.abspath(html_output)
        down_output = os.path.abspath(down_output)
        page_size = self.conf['page_size']
        badtitle = self.conf['unwanted_titles']
        output = os.path.abspath(self.Options.output)
        db = os.path.abspath(self.Options.db)
        rawcsv = os.path.abspath(self.Options.rawcsv)
        self.log.debug(f'Download dir for markdown: {self.downdir}')
        log = self.log
        opts = self.Options
        match opts.action:
            case 'fetch':
                fech = FetchSpeeches()
                fech.gen_entry_file(username, password, badtitle, self.priv,
                                    down_output, page_size, start,
                                    end, db, log)
            case 'vispage':
                geml = GenHTML()
                # TODO: Fix this. Not enough params to create a timeline
                geml.create_timeline(down_output, html_output, title)
            case 'timeline':
                timl = Timeline()
                # TODO: Fix this. Not enough params to create a timeline
                timl.create_timeline(down_output)
            case 'markdown':
                mktbl = MakeTables()
                mktbl.create_markdown(db, output, self.downdir, self.priv,
                                      self.webpath, self.link, log)
            case 'download':
                down = DownSpeeches()
                down.get_speeches(db, self.downdir, username, password, log)
            case 'raw':
                fech = FetchSpeeches()
                fech.gen_raw(username, password, rawcsv, log)
            case 'extract':
                exfle = ExtractFiles()
                exfle.extract(downdir=self.downdir, exft=self.exft, tdir=self.tdir,
                              log=log)
            case _:
                parse.print_help()


if __name__ == '__main__':
    piotr = Piotr(Options)
    piotr.main()
