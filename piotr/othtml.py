#!/usr/bine/env python3
# -*- coding: utf-8 -*-
# MIT License = 'Copyright (c) 2024 Anoduck'
# This software is released under the MIT License.
# https: //opensource.org/licenses/MIT
from fetch import FetchSpeeches
import pandas as pd
import sys


class GenHTML:
    
    def __init__(self):
        self.fech = FetchSpeeches()

    def generate_html(self, entries, title, grp_ls):
        grp_short = list(set(grp_ls))
        my_len = len(grp_ls)
        group_range = range(0, my_len)
        start_list = entries.start.to_list()
        content_list = entries.content.to_list()
        end_list = entries.end.to_list()
        spkid_list = entries.speech_id.to_list()

        """
        see also: https://visjs.org/
        """
        pagetitle = title
        html_content = f"""<!DOCTYPE HTML>
    <html>
    <head>
    <title>{pagetitle}</title>
    <style type="text/css">
        body, html {{
        font-family: sans-serif;
        }}
        .vis-item .vis-item-overflow {{
        overflow: visible;
        }}
    </style>
    <script type="text/javascript" src="https://unpkg.com/moment@latest"></script>
    <script type="text/javascript" src="https://unpkg.com/jquery@latest"></script>
    <script type="text/javascript" src="https://unpkg.com/vis-data@latest/peer/umd/vis-data.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/vis-timeline@latest/peer/umd/vis-timeline-graph2d.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://unpkg.com/vis-timeline/styles/vis-timeline-graph2d.min.css" />
    <!-- Include other packages like Vis Network or Vis Graph3D here. -->
    <!-- Optionally include locales for Moment if needing any. -->
    <!-- alternative ways:
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css" />
    -->
    </head>
    <body onresize="/*timeline.checkResize();*/">
    <div id="title">
        <h1>{pagetitle}</h1>
        <br>
        <h2> Timeline Usage </h2>
        <br>
        <p>The timeline is dynamic and interactive. In order to view all of it's contents, you will need
        to interact with it on some basis. You can scroll both up and down, and to the left and
        right. Hovering over objects also will reveal some information. </p>
        <p>There are approximately 314 events recorded and spans three years. The events are grouped by
        keywords that indicate what the event was about. Currently the audio log of the event is not
        accessible from this interface, and will have to be worked into availability at some point in time.</p>
        <br>
        <h2>Instructions</h2>
        <br>
        <p> The timeline works as follows: </p>
        <br>
        <ol>
            <li> This timeline is click activated. so, YOU MUST CLICK TO USE! </li>
            <li>Scrolling with the mouse wheel will both propel you forward in time and scroll down the
        keyword list</li>
            <li>Zoom is controled through use of the mouse wheel.</li>
            <li>Panning is controled through the mouse.</li>
            <li>Hovering over objects will display an event id.</li>
            <li>If you have trouble viewing the timeline, please reload the page and check if you have
        any ad blockers preventing it from fully loading.</li>
        </ol>
        <br>
        <br>
    <div id="visualization"></div>
    <script type="text/javascript">
    """

        html_content += """
        function showVisibleItems() {
            var visibleItems = timeline.getVisibleItems();
            document.getElementById("visibleItemsContainer").innerHTML = ""
            document.getElementById("visibleItemsContainer").innerHTML += visibleItems;
        };
        var count = 1000;
        """

        html_content += f"""
    var groups = new vis.DataSet(["""
        for i in grp_short:
            grps = ''.join(i.split())
            html_content += f"""
    {{ id: '{grps}' }},"""
        html_content += f"""
    ]);"""

        html_content += f"""
    var items = new vis.DataSet(["""
        # for idx, row in entries.iterrows():
        for idx in group_range:
            if content_list[idx] == '' or start_list[idx] == '':
                print("Error. The 'content' or 'start' columns cannot be empty.")
                sys.exit(1)
            item_id = f"id: {idx}"
            group_item = grp_ls[idx]
            grp = ''.join(group_item.split())
            item_group = f", group: '{grp}'"
            item_title = f", title: '{spkid_list[idx]}'"
            element_content = content_list[idx]
            new_content = ' '.join(element_content.split()[:10])
            item_content = f", content: '{new_content}'"
            item_start = f", start: '{start_list[idx]}'"
            item_end = "" if end_list[idx] == '' else f", end: '{end_list[idx]}'"
            html_content += f"""
        {{{item_id}{item_group}{item_title}{item_content}{item_start}{item_end}}},"""

    # Disabled align-dynamic and dataAttributes-all and cluster - true
        html_content += f"""
    ]);
    var options = {{
        height: '600px',
        maxHeight: '800px',
        verticalScroll: true,
        horizontalScroll: true,
        zoomKey: 'ctrlKey',
        min: '2019-12-31',
        max: '2023-08-15',
        stack: true,
        type: 'point',
        clickToUse: true,
        dataAttributes: 'all'
    }};"""

        html_content += f"""
    var container = document.getElementById('visualization');
    var timeline = new vis.Timeline(container, items, options);"""
        html_content += f"""
    timeline.setGroups(groups);
    timeline.setItems(items);
    """
        html_content += f"""
    </script>
    </body>
    </html>"""
        return html_content
    

    def create_timeline(self, down_output, html_output, title):
        entries = pd.read_csv(down_output)
        fetch = FetchSpeeches()
        group_list, bad_rows = fetch.gen_groups(entries)
        if len(bad_rows) > 0:
            entries = entries.drop(axis=0, index=bad_rows)
        html_content = self.generate_html(entries, title, group_list)
        rit_html = open(html_output, mode='w')
        rit_html.writelines(html_content)
    